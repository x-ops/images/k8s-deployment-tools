ARG KUBECTL_VERSION
ARG HELM_VERSION
ARG HELMFILE_VERSION

# --- Base

FROM docker.io/library/alpine:3.17.1 AS base

# --- Builder

FROM base AS builder

ARG KUBECTL_VERSION
ARG HELM_VERSION
ARG HELMFILE_VERSION

ENV KUBECTL_VERSION=${KUBECTL_VERSION}
ENV HELM_VERSION=${HELM_VERSION}
ENV HELMFILE_VERSION=${HELMFILE_VERSION}

ENV KUBECTL_BINARY_URL="https://dl.k8s.io/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl"
ENV HELM_PACKAGE="helm-v${HELM_VERSION}-linux-amd64.tar.gz"
ENV HELM_PACKAGE_URL="https://get.helm.sh/${HELM_PACKAGE}"
ENV HELMFILE_PACKAGE="helmfile_${HELMFILE_VERSION}_linux_amd64.tar.gz"
ENV HELMFILE_PACKAGE_URL="https://github.com/helmfile/helmfile/releases/download/v${HELMFILE_VERSION}/${HELMFILE_PACKAGE}"

RUN mkdir -p /install/bin

ENV PATH="/install/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

RUN set -eux \
  ; wget -qO kubectl "$KUBECTL_BINARY_URL" \
  ; install -o root -g root -m 0755 -c kubectl /install/bin/kubectl \
  ; kubectl version --short --client

RUN set -eux \
  ; wget -qO "$HELM_PACKAGE" "$HELM_PACKAGE_URL" \
  ; tar -zxvf "$HELM_PACKAGE" \
  ; install -o root -g root -m 0755 -c linux-amd64/helm /install/bin/helm \
  ; helm version --short

RUN set -eux \
  ; wget -qO "$HELMFILE_PACKAGE" "$HELMFILE_PACKAGE_URL" \
  ; tar -zxvf "$HELMFILE_PACKAGE" \
  ; install -o root -g root -m 0755 -c helmfile /install/bin/helmfile \
  ; helmfile --version

# --- Runtime

FROM base

RUN set -eux \
  ; apk add --no-cache --quiet \
        bash \
        curl \
        git \
        jq \
        unzip \
  ; curl --version \
  ; git --version \
  ; jq --version \
  ; adduser -h /home/ci -g "CI" -s /bin/bash -D -u 1042 -D ci \
  ; rm -rf \
    /tmp/* \
    /var/tmp/* \
    /var/lib/apk/* \
    /var/cache/apk/* \
    /usr/share/git-core/templates \
  ; rm -f \
    ~/.ash_history \
    ~/.bash_logout \
    ~/.bash_history \
    ~/.wget-hsts \
    /home/ci/.ash_history \
    /home/ci/.bash_history \
    /home/ci/.bash_logout

COPY --from=builder /install /

USER 1042:1042

RUN set -eux \
  ; helm plugin install https://github.com/databus23/helm-diff \
  ; rm -rf \
    ~/.local/share/helm/plugins/helm-diff/.circleci \
    ~/.local/share/helm/plugins/helm-diff/.editorconfig \
    ~/.local/share/helm/plugins/helm-diff/.git \
    ~/.local/share/helm/plugins/helm-diff/.gitattributes \
    ~/.local/share/helm/plugins/helm-diff/.github \
    ~/.local/share/helm/plugins/helm-diff/.gitignore \
    ~/.local/share/helm/plugins/helm-diff/cmd \
    ~/.local/share/helm/plugins/helm-diff/CONTRIBUTING.md \
    ~/.local/share/helm/plugins/helm-diff/diff \
    ~/.local/share/helm/plugins/helm-diff/go.mod \
    ~/.local/share/helm/plugins/helm-diff/go.sum \
    ~/.local/share/helm/plugins/helm-diff/install-binary.sh \
    ~/.local/share/helm/plugins/helm-diff/main.go \
    ~/.local/share/helm/plugins/helm-diff/Makefile \
    ~/.local/share/helm/plugins/helm-diff/manifest \
    ~/.local/share/helm/plugins/helm-diff/README.md \
    ~/.local/share/helm/plugins/helm-diff/scripts \
    ~/.local/share/helm/plugins/helm-diff/testdata \
    /tmp/* \
    /var/tmp/* \
  ; helm plugin list \
  ; helm diff version \
  ; rm -rf ~/.cache \
  ; rm -f ~/.ash_history ~/.bash_history ~/.wget-hsts

CMD ["bash"]
