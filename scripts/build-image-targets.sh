#!/usr/bin/env bash

set -eu

getImage() {
    local registry repository version
    version=${KUBECTL_VERSION}

    if [ -n "${CI:-}" ] && [ -n "${CI_REGISTRY_IMAGE:-}" ]; then
        registry=$(echo "$CI_REGISTRY_IMAGE" | cut -d '/' -f 1)
        repository=$(echo "$CI_REGISTRY_IMAGE" | cut -d '/' -f 2-)
    else
        registry=${IMAGE_REGISTRY:-"registry.gitlab.com"}
        repository=${IMAGE_REPOSITORY:-"x-ops/images/k8s-deployment-tools"}
    fi

    echo "$registry/$repository:$version"
}

if [ $# -eq 0 ]; then
    echo "Usage: $(basename "$0") <Kubernetes version>" >&2
    exit 1
else
    export KUBECTL_VERSION="$1"
    shift
fi

HELM_VERSION=${HELM_VERSION:-"3.10.3"}
HELMFILE_VERSION=${HELMFILE_VERSION:-"0.149.0"}
export HELM_VERSION HELMFILE_VERSION

tag=$(getImage)

docker build . --tag "$tag" \
    --build-arg KUBECTL_VERSION \
    --build-arg HELM_VERSION \
    --build-arg HELMFILE_VERSION \
    --no-cache "$@"

docker push "$tag"
